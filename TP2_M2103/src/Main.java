
public class Main {
	
	final static Rationnel base = new Rationnel(2, 1);

	public static void main(String[] args) {
		Rationnel result = base;
		Rationnel demi = new Rationnel(1, 2);
		Rationnel calcul;
		
		for (int i = 0; i < 4; i++) {
			calcul = base.division(result);
			calcul = calcul.somme(result);
			result = demi.produit(calcul);
		}
		
		System.out.println("Valeur approché de racine carré de " + base.getNumerateur() + " est: " + result);
		System.out.println("Valeur approché de racine carré de " + base.getNumerateur() + " est: " + result.getNumerateur()/(double) result.getDenominateur());
	}

}
