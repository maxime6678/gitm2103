
public class Couleur {
	
	private int nuanceRouge;
	private int nuanceVert;
	private int nuanceBleu;
	
	public Couleur(int nuanceRouge, int nuanceVert, int nuanceBleu) throws IllegalArgumentException {
		if (nuanceRouge < 0 || nuanceRouge > 255 || nuanceVert < 0 || nuanceVert > 255 || nuanceBleu < 0 || nuanceBleu > 255) throw new IllegalArgumentException();
		this.nuanceRouge = nuanceRouge;
		this.nuanceVert = nuanceVert;
		this.nuanceBleu = nuanceBleu;
	}
	
	public int getNuanceRouge() {
		return nuanceRouge;
	}
	
	public void setNuanceRouge(int nuanceRouge) throws IllegalArgumentException {
		if (nuanceRouge < 0 || nuanceRouge > 255) throw new IllegalArgumentException();
		this.nuanceRouge = nuanceRouge;
	}

	public int getNuanceVert() {
		return nuanceVert;
	}

	public void setNuanceVert(int nuanceVert) throws IllegalArgumentException {
		if (nuanceVert < 0 || nuanceVert > 255) throw new IllegalArgumentException();
		this.nuanceVert = nuanceVert;
	}

	public int getNuanceBleu() {
		return nuanceBleu;
	}

	public void setNuanceBleu(int nuanceBleu) throws IllegalArgumentException {
		if (nuanceBleu < 0 || nuanceBleu > 255) throw new IllegalArgumentException();
		this.nuanceBleu = nuanceBleu;
	}
	
	public int valeurRGB() {
		return nuanceRouge * 256 * 256 + nuanceVert * 256 + nuanceBleu;
	}
	
	public String toString() {
		return "[" + this.nuanceRouge  + "," + this.nuanceVert + "," + this.nuanceBleu + "]";
	}

	public static Couleur getRouge() {
		return new Couleur(255, 0, 0);
	}
	
	public static Couleur getVert() {
		return new Couleur(0, 255, 0);
	}
	
	public static Couleur getBleu() {
		return new Couleur(0, 0, 255);
	}

}
