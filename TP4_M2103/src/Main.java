
public class Main {

	public static void main(String[] args) {
		Point a = new Point(1, 5);
		Point b = new Point(2, 4);
		Segment s = new Segment(a, b);
		System.out.println("Le milieu du segment a pour coordonnee: " + s.pointMilieu());

	}

}
