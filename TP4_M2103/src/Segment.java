
public class Segment {
	
	private Point origine;
	private Point extremite;
	
	public Segment(Point origine, Point extremite) {
		this.origine = origine;
		this.extremite = extremite;
	}
	
	public Point pointMilieu() {
		Point milieu = this.origine;
		milieu.translater(this.extremite.getAbscisse(), this.extremite.getOrdonnee());
		milieu.mettreAEchelle((float) 1/2);
		return milieu;
	}

}
