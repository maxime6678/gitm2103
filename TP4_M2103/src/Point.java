
public class Point {
	
	private double abscisse;
	private double ordonnee;
	
	public Point(double abscisse, double ordonnee) {
		this.abscisse = abscisse;
		this.ordonnee = ordonnee;
	}

	public double getAbscisse() {
		return abscisse;
	}

	public double getOrdonnee() {
		return ordonnee;
	}
	
	public void translater(double tx, double ty) {
		this.abscisse = this.abscisse + tx;
		this.ordonnee = this.ordonnee + ty;
	}
	
	public void mettreAEchelle(double h) throws IllegalArgumentException {
		if (h <= 0) throw new IllegalArgumentException();
		this.abscisse = this.abscisse * h;
		this.ordonnee = this.ordonnee * h;
	}
	
	public String toString() {
		return "(" + this.abscisse + ";" + this.ordonnee + ")";
	}
	
	public static Point pointOrigine() {
		return new Point(0.0, 0.0);
	}

}
