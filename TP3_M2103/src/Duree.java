
public class Duree {
	
	private final long h;
	private final long m;
	private final long s;
	
	public Duree(long h, long m, long s) throws IllegalArgumentException {
		if (h < 0 || m >= 60 || m < 0 || s >= 60 || s < 0) throw new IllegalArgumentException();
		this.h = h;
		this.m = m;
		this.s = s;
	}
	
	public long getHeures() {
		return this.h;
	}
	
	public long getMinutes() {
		return this.m;
	}
	
	public long getSecondes() {
		return this.s;
	}
	
	public boolean egal(Duree d) {
		return (this.h == d.h && this.m == d.m && this.s == d.s);
	}
	
	public boolean inf(Duree d) {
		return (this.h < d.h || (this.h == d.h && this.m < d.m) || (this.h == d.h && this.m == d.m && this.s < d.s));
	}
	
	public Duree ajouterUneSeconde() {
		if (this.s == 59) {
			if (this.m == 59) {
				return new Duree(this.h + 1, 0, 0);
			} else {
				return new Duree(this.h, this.m + 1, 0);
			}
		} else {
			return new Duree(this.h, this.m, this.s + 1);
		}
	}
	
	public String toString() {
		return this.h + ":" + this.m + ":" + this.s;
	}

}
