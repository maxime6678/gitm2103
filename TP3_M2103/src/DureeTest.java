import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class DureeTest {

    @Test
    public void testGetters() {
        Duree d = new Duree(1, 2, 3);
        assertEquals(1, d.getHeures());
        assertEquals(2, d.getMinutes());
        assertEquals(3, d.getSecondes());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testHeuresNegative() {
        new Duree(-1, 2, 3);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testMinutesNegative() {
        new Duree(1, -2, 3);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testMinutesSuperieur59() {
        new Duree(1, 60, 3);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testSecondesNegative() {
        new Duree(1, 2, -3);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testSecondesSuperieur59() {
        new Duree(1, 2, 60);
    }

    @Test
    public void testEgal() {
        assertTrue(new Duree(1, 2, 3).egal(new Duree(1, 2, 3)));
    }

    @Test
    public void testNonEgal() {
        assertFalse(new Duree(1, 2, 3).egal(new Duree(2, 2, 3)));
        assertFalse(new Duree(1, 2, 3).egal(new Duree(1, 1, 3)));
        assertFalse(new Duree(1, 2, 3).egal(new Duree(1, 2, 2)));
    }

    @Test
    public void testAjouterUneSeconde() {
        Duree d123 = new Duree(1, 2, 3);
        d123 = d123.ajouterUneSeconde();
        assertTrue(new Duree(1, 2, 4).egal(d123));
        Duree d1259 = new Duree(1, 2, 59);
        d1259 = d1259.ajouterUneSeconde();
        assertTrue(new Duree(1, 3, 0).egal(d1259));
        Duree d5959 = new Duree(1, 59, 59);
        d5959 = d5959.ajouterUneSeconde();
        assertTrue(new Duree(2, 0, 0).egal(d5959));
    }

    @Test
    public void testInf() {
        assertTrue(new Duree(1, 2, 3).inf(new Duree(2, 2, 3)));
        assertTrue(new Duree(1, 2, 3).inf(new Duree(1, 3, 3)));
        assertTrue(new Duree(1, 2, 3).inf(new Duree(1, 2, 4)));
    }

    @Test
    public void testNonInf() {
        assertFalse(new Duree(1, 2, 3).inf(new Duree(1, 2, 3)));
        assertFalse(new Duree(2, 2, 3).inf(new Duree(1, 2, 3)));
        assertFalse(new Duree(1, 3, 3).inf(new Duree(1, 2, 3)));
        assertFalse(new Duree(1, 2, 4).inf(new Duree(1, 2, 3)));
    }

    @Test
    public void testToString() {
        assertEquals("1:2:3", new Duree(1, 2, 3).toString());
    }
}
