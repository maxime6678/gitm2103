
public class Main {

	public static void main(String[] args) {
		NbNS nombre1 = new NbNS(5.06, 15);
		NbNS nombre2 = new NbNS(3.2, -5);
		
		System.out.println("l'ordre de grandeur du nombre " + nombre1 + " est de " + nombre1.ordreGrandeur() + " .");
		System.out.println("l'ordre de grandeur du nombre " + nombre2 + " est de " + nombre2.ordreGrandeur() + " .");		
		
		NbNS produit = nombre1.produit(nombre2);
		System.out.println("Le produit des deux est de : " + produit );
		
		produit = nombre2;
		for (int i = 0; i < 4; i++) {
			produit = produit.produit(nombre2);
		}
		
		System.out.println(nombre2 + " à la puissance 5 = " + produit);
		
		// Le réel 𝟓𝟒𝟓𝟓 permet d’obtenir le nombre en notation scientifique 𝟓, 𝟒𝟓𝟓 × 𝟏𝟎𝟑.
		NbNS reel = new NbNS(5455);
		System.out.println("Le réel 5455 permet d'obtenir le nombre: " + reel);
		
		//Le réel 𝟎, 𝟎𝟎𝟎𝟕𝟖𝟕 permet d’obtenir le nombre en notation scientifique 𝟕, 𝟖𝟕 × 𝟏𝟎􏰀𝟒.
		NbNS reel2 = new NbNS(0.000787);
		System.out.println("Le reel 0.000787 permet : " + reel2 );
		

	}

}
