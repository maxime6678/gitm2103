
public class NbNS {
	
	/**
     * mantisse du nombre scientifique
     */
	private double mantisse;
	
	/**
     * exposant du nombre scientifique
     */
	private int exposant;
	
	/**
	 * construit un nombre scientifique
	 * @param a mantisse du nombre scientifique
	 * @param n exposant du nombre scientifique
	 * @throws IllegalArgumentException si 1 < mantisse <= 10
	 */
	public NbNS(double a, int n) throws IllegalArgumentException {
		if (a < 1 || a  >= 10) {
			throw new IllegalArgumentException("Mantisse incorrecte");
		}
		this.mantisse = a;
		this.exposant = n;
	}
	
	public NbNS(double a) throws IllegalArgumentException {
		if (a <= 0) {
			throw new IllegalArgumentException("Réel incorrect");
		}
		
		double mantisse = a;
		int exposant = 0;
		
		while (!(mantisse <= 1 || mantisse < 10)) {
			if (mantisse < 1) {
				mantisse *= 10;
				exposant--;
			} else {
				mantisse /= 10;
				exposant++;
			}
		}
		
		this.mantisse = mantisse;
		this.exposant = exposant;
		
		// NbNS2(a, 0); par recursivité
	}
	
	private void NbNS2(double a, int n) {
		if (a <= 1 || a < 10) {
			this.mantisse = a;
			this.exposant = n;
		} else if (a < 1) {
			this.NbNS2(a*10, n-1);
		} else {
			this.NbNS2(a/10, n+1);
		}
	}
	
	/**
	 * retourne la mantisse du nombre scientique
	 * @return mantisse
	 */
	public double getMantisse() {
		return this.mantisse;
	}
	
	/**
	 * retourne l'exposant du nombre scientique
	 * @return exposant
	 */
	public int getExposant() {
		return this.exposant;
	}
	
	/**
	 * Retourne l'ordre de grandeur du nombre scientifique
	 * @return nombre scientique sous forme 1x10^exposant
	 */
	public NbNS ordreGrandeur() {
		if (this.getMantisse() > 5.0) {
			return new NbNS(1, this.exposant+1);
		}
		return new NbNS(1, this.exposant);
	}
	
	/**
	 * Calcule le produit de deux nombres scientifques
	 * @param nb deuxieme operande du produit
	 * @return le nombre scientique produit des deux autres
	 */
	public NbNS produit(NbNS nb) {
		if (this.getMantisse() * nb.getMantisse() < 10) {
			return new NbNS(this.getMantisse() * nb.getMantisse(), this.getExposant() + nb.getExposant());
		}
		return new NbNS(this.getMantisse()*nb.getMantisse()/10, this.getExposant()+nb.getExposant()+1);
	}
	
	/**
	 * retourne la chaine de caracteres correspondante au nombre scientifique attribue
	 * @return nombre scientifique sous forme de chaine de caracteres
	 */
	@Override
	public String toString() {
		if (this.getExposant()>0) {
			return this.getMantisse() + " E+" + this.getExposant();
		} else if (this.getExposant()<0) {
			return this.getMantisse() + " E" + this.getExposant();
		}
		return this.getMantisse() + "";
		
	}

}
