import java.util.Scanner;

public class Asterisques {

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		
		System.out.println("Valeur de n :");
		int n = scan.nextInt();
		
		for (int i = 0; i < n; i++) {
			for (int j = i; j >= 0; j--) {
				System.out.print("*");
			}
			System.out.println();
		}
		
		scan.close();
	}

}

