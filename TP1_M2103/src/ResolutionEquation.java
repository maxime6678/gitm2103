import java.util.Scanner;

public class ResolutionEquation {

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		
		int a = 0;
		do {
			System.out.println("Coefficient a :");
			a = scan.nextInt();
		} while (a == 0);
		
		System.out.println("Coefficient b :");
		int b = scan.nextInt();
		
		System.out.println("Coefficient c :");
		int c = scan.nextInt();
		
		System.out.println("Résolution de l'équation : " + a + "x^2 + " + b + "x + " + c + " = 0");
		double delta = Math.pow(b, 2)-4*a*c;
		System.out.println("Discriminant : " + delta);
		
		if (delta > 0) {
			System.out.println("2 solutions : " + (-b+Math.sqrt(delta))/(2*a) + " et " + (-b-Math.sqrt(delta))/(2*a));
		} else if (delta == 0) {
			System.out.println("1 solution : " + -b/(2*a));
		} else {
			System.out.println("Aucune solution réelle");
		}
		
		scan.close();
	}

}
